$(document).ready(function(){	
	
	// Hides Innovatives annoying footer
	$('span:contains(WebPAC)').hide();
	
	// Enables Tabs
    $('div#header > div.container > ul').tabs({ fx: { height: 'toggle', opacity: 'toggle' } });	
	
	$('.searchbox').bind('blur', function() {
		
		var newValue = $(this).attr('value');
		
		if (!newValue) {
			newValue = "";
			}
		
		$('.searchbox').each(function(){
			$(this).attr('value', newValue)
		});
		
		$('.textbox:first').attr('value', newValue);
		
	});
	
	// Disables Refined Search
	$('div#header > div.container > ul').bind('tabsshow', function(event, ui) {
		var tabID = ui.tab.toString();
		var tabIDSplit = tabID.split("#");
		if (tabIDSplit[1] == "advancedsearch") {
			var selectedTabNew = "#" + tabIDSplit[1] + " .textbox:first";
		} else {
			var selectedTabNew = "#" + tabIDSplit[1] + " .searchbox";			
		}
		
		$(selectedTabNew).focus();											   
	}); 
	
	window.onload = function () {
		$("#searchinfo .searchbox").focus();
		setFooter();
	}

  });